﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public class CustomerSpender
    {
        public string CustomerName { get; set; }
        public decimal CustomerSpending { get; set; }
    }
}
