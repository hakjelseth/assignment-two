﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public interface ICustomerSpenderRepository
    {

        /// <summary>
        /// Finds out what all the customers have spent and puts it in a descending list.
        /// </summary>
        /// <returns>A list of CustomerSpender objects</returns>
        public List<CustomerSpender> GetCustomerSpending();
    }
}
