﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public interface ICustomerGenreRepository
    {
        /// <summary>
        /// For a specific customer, it finds their most popular music genre.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Returns a CustomerGenre object</returns>
        public CustomerGenre GetCustomersMostPopularGenre(Customer customer);
    }
}
