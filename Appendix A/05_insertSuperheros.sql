INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Bruce Wayne', 'Batman', 'Gotham');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Clark Kent', 'Superman', 'Krypton');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Peter Parker', 'Spiderman', 'NYC');