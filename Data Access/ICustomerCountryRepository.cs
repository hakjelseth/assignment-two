﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public interface ICustomerCountryRepository
    {
        /// <summary>
        /// GetCustomersPerCountry makes a list with every country and the amount of customers they have.
        /// </summary>
        /// <returns>A list of CustomerCountry objects</returns>
        public List<CustomerCountry> GetCustomersPerCountry();
    }
}
