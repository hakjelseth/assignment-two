CREATE TABLE SuperheroPowerRelationship(
	SuperheroID int,
	PowerID int,
	FOREIGN KEY (SuperheroID) REFERENCES Superhero(Id),
	FOREIGN KEY (PowerId) REFERENCES Power(Id),
	PRIMARY KEY(SuperheroID, PowerID)
)
