﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;

namespace Assignment_Two
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerRepository customRep = new CustomerRepository();
            CustomerGenreRepository customGenreRep = new CustomerGenreRepository();
            Customer testCustomer = customRep.GetCustomerByID("12");

            /*List<CustomerSpender> liste = customRep.GetCustomerSpending();
            
            foreach (CustomerSpender person in liste)
                Console.WriteLine(person.CustomerName + " " + person.CustomerSpending);*/
            /*
            Customer customer = new Customer()
            {
                CustomerID = 60,
                FirstName = "Dawit",
                LastName = "Chala",
                Country = "NOReg",
                PostalCode = "0170",
                PhoneNumber = "320302302",
                Email = "hakjelseth@gmail.com"
            };
            //Console.WriteLine(customRep.AddNewCustomer(customer));

            //Console.WriteLine(customRep.UpdateCustomer(customer));
            */
            foreach(string genre in customGenreRep.GetCustomersMostPopularGenre(testCustomer).GenreName)
                Console.WriteLine(genre);

        }

        private static void ReadOrderData(string connectionString, string queryString)
        {

            using (SqlConnection connection = new SqlConnection(
                       connectionString))
            {
                SqlCommand command = new SqlCommand(
                    queryString, connection);
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(String.Format("{0}, {1}",
                            reader[0], reader[1]));
                    }
                }
            }
        }
    }
}
