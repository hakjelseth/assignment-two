﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public interface ICustomerRepository
    {

        /// <summary>
        /// Finds a customer by their customerID.
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns>A customer object</returns>
        public Customer GetCustomerByID(string id);

        /// <summary>
        /// Finds a customer by their first name.
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns>A customer object</returns>
        public Customer GetCustomerByName(string name);

        /// <summary>
        /// Finds all customers with an offset and a limit.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>Returns a list with customer objects.</returns>
        public List<Customer> GetAllCustomers(int limit, int offset);

        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Returns true if successful, false if not</returns>
        public bool AddNewCustomer(Customer customer);

        /// <summary>
        /// Updates an existing customer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Returns true if successful, false if not.</returns>
        public bool UpdateCustomer(Customer customer);

        /// <summary>
        /// Deletes a customer.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns true if successful, false if not</returns>
        public bool DeleteCustomer(string id);
    }
}
