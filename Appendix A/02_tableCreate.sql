CREATE TABLE Superhero(
	Id int NOT NULL IDENTITY(1,1),
	Name nvarchar(255),
	Alias nvarchar(255),
	Origin nvarchar(255),
	PRIMARY KEY(Id)
)

CREATE TABLE Assistant(
	Id int NOT NULL IDENTITY(1,1),
	Name nvarchar(255),
	PRIMARY KEY(Id)
)

CREATE TABLE Power(
	Id int NOT NULL IDENTITY(1,1),
	Name nvarchar(255),
	Description nvarchar(255),
	PRIMARY KEY(Id)
)