﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Finds out what all the customers have spent and puts it in a descending list.
        /// </summary>
        /// <returns>A list of CustomerSpender objects</returns>
        public List<CustomerSpender> GetCustomerSpending()
        {
            List<CustomerSpender> customerSpenderList = new();
            string sql = "SELECT FirstName +  ' ' + LastName as Name, sum(Total) as 'Total spend '" +
                "From dbo.Customer Inner join dbo.Invoice on dbo.Customer.CustomerId = dbo.Invoice.Customerid " +
                "Group by dbo.Customer.CustomerId, dbo.Customer.FirstName, dbo.Customer.LastName " +
                    "ORDER BY 'Total spend' DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender();
                                customerSpender.CustomerName = reader.GetString(0);
                                customerSpender.CustomerSpending = reader.GetDecimal(1);
                                customerSpenderList.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                // Log to error
            }
            return customerSpenderList;
        }
    }
}
