﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// For a specific customer, it finds their most popular music genre.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Returns a CustomerGenre object</returns>
        public CustomerGenre GetCustomersMostPopularGenre(Customer customer)
        {
            CustomerGenre customerGenre = new CustomerGenre();

            string sql = "SELECT Genre.Name " +
                            "FROM dbo.Customer INNER JOIN dbo.Invoice on dbo.Customer.CustomerId = dbo.Invoice.CustomerId " +
                            "Inner Join dbo.InvoiceLine on dbo.Invoice.InvoiceId = dbo.InvoiceLine.InvoiceId " +
                            "Inner Join dbo.Track on dbo.InvoiceLine.TrackId = dbo.Track.TrackId " +
                            "Inner Join dbo.Genre on dbo.Track.GenreId = dbo.Genre.GenreId " +
                            "WHERE dbo.Customer.CustomerId = @customerId " +
                            "GROUP BY Genre.Name " +
                            "HAVING COUNT(Genre.Name) = (SELECT MAX(teller) " +
                            "FROM(SELECT Genre.Name as genre, COUNT(Genre.Name) as teller " +
                            "FROM dbo.Customer INNER JOIN dbo.Invoice on dbo.Customer.CustomerId = dbo.Invoice.CustomerId " +
                            "Inner Join dbo.InvoiceLine on dbo.Invoice.InvoiceId = dbo.InvoiceLine.InvoiceId " +
                            "Inner Join dbo.Track on dbo.InvoiceLine.TrackId = dbo.Track.TrackId " +
                            "Inner Join dbo.Genre on dbo.Track.GenreId = dbo.Genre.GenreId " +
                            "WHERE dbo.Customer.CustomerId = @customerId " +
                            "GROUP BY Genre.name) f)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerID", customer.CustomerID);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customerGenre.GenreName.Add(reader.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                // Log to error
            }
            return customerGenre;
        }
    }
}
