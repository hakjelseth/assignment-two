# Assignment Two

This is the solution to assignment two. 
The assignment was split in two appendixes, A and B. Appendix A contains 9 SQL scripts, whilst appendix B uses .Net libraries to interact with a database.

## Requirements
* .NET Core 5.0
* Chinook DB
* Microsoft  SQL Server Management Studio 

Environment variable named chinook_data_source which is the data source for the DB.

  

## Participants
This assignment was completed by **Dawit Chala** and **Håkon Kjelseth**.
