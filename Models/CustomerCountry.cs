﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int NumberOfCustomers { get; set; }
    }
}
