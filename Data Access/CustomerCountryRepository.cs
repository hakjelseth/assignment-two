﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Two
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// GetCustomersPerCountry makes a list with every country and the amount of customers they have.
        /// </summary>
        /// <returns>A list of CustomerCountry objects</returns>
        public List<CustomerCountry> GetCustomersPerCountry()
        {
            List<CustomerCountry> customerCountryList = new();
            string sql = "SELECT Country, count(Country) as Customers FROM dbo.Customer " +
                "GROUP BY Country ORDER BY Customers DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.CountryName = reader.GetString(0);
                                customerCountry.NumberOfCustomers = reader.GetInt32(1);
                                customerCountryList.Add(customerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                // Log to error
            }
            return customerCountryList;
        }
    }
}
