INSERT INTO Power(Name, Description)
VALUES ('Superstrength', 'really strong');

INSERT INTO SuperheroPowerRelationship(SuperheroID, PowerID)
VALUES (2, 1);


INSERT INTO Power(Name, Description)
VALUES ('Invisibility', 'cant be seen');

INSERT INTO SuperheroPowerRelationship(SuperheroID, PowerID)
VALUES (2, 2);

INSERT INTO Power(Name, Description)
VALUES ('Mindreader', 'can read minds');

INSERT INTO SuperheroPowerRelationship(SuperheroID, PowerID)
VALUES (3, 3);


INSERT INTO Power(Name, Description)
VALUES ('Superfarter', 'the smell can kill');

INSERT INTO SuperheroPowerRelationship(SuperheroID, PowerID)
VALUES (1, 4);

INSERT INTO SuperheroPowerRelationship(SuperheroID, PowerID)
VALUES (2, 4);
